import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { CalcImcPage } from '../calc-imc/calc-imc';
import { TabelaPage } from '../tabela/tabela';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CalcImcPage;
  tab3Root = TabelaPage

  constructor() {

  }
}
