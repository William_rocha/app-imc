import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalcImcPage } from './calc-imc';

@NgModule({
  declarations: [
    CalcImcPage,
  ],
  imports: [
    IonicPageModule.forChild(CalcImcPage),
  ],
})
export class CalcImcPageModule {}
