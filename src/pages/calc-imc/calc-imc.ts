import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-calc-imc',
  templateUrl: 'calc-imc.html',
})
export class CalcImcPage {
  nome: string;
  altura: number;
  data: Date;
  idade: number;
  peso: number;
  imc: number;
  sexo: string;
  mensagem: Array<string> = [];//Decidi criar um vetor de mensagem para não ter que ficar criando varios
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {

  }
  presentAlerVazio() {
    let alert = this.alertCtrl.create({
      title: "Preencha todos os campos para que possa ser calculado o seu IMC",
      buttons: ['Ok']
    });
    alert.present();
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: this.nome + ", seu Índice de Massa Corporal é de: " + this.imc,
      subTitle: 'Você está ' + this.mensagem[0],
      message: this.mensagem[1],
      buttons: ['Ok']
    });
    alert.present();
  }

  calcularIdade() {
    var anoAtual = new Date()
    var anoNascimento = new Date(this.data);
    var diferenca = anoAtual.getTime() - anoNascimento.getTime();
    this.idade = Math.floor((diferenca / (1000 * 3600 * 24)) / 365.25);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalcImcPage');
  }

  /*opcao_mensagem(_mensagem:string):string{
    if(_mensagem == 'no seu peso ideal, Parabéns!!!')
      return 'Parabéns, você está em seu no seu peso ideal, Parabéns!!!';
    else if(_mensagem == 'Abaixo do peso!')
      return 'Você está abaixo do seu no seu peso ideal, Parabéns!!!';
    else if(_mensagem == "Abaixo do peso!")
      return 'Cuidado, Você está muito abaixo do seu no seu peso ideal, Parabéns!!!';
    else if(_mensagem == 'Pouco acima do peso!')
      return 'Sobrepeso - Você está acima do seu peso ideal.';
    else if(_mensagem == "Obesidade!")
      return 'Obesidade';
  }*/

  calc_imc(sexo_option: string) {
    if (this.nome  == undefined || this.sexo == undefined || this.data == undefined || this.altura == undefined || this.peso == undefined) {
      this.presentAlerVazio();
    } else {
      let _imc = this.peso / (this.altura * this.altura);
      this.imc = parseFloat(_imc.toFixed(2));
      this.calcularIdade();
      if (sexo_option == 'f') {
        this.imc_feminino(this.imc);
      } else if (sexo_option == 'm') {
        this.imc_masculino(this.imc);
      }
      this.presentAlert();
    }
  }

  imc_masculino(valor_imc: number) {
    if (valor_imc < 17.5 && this.idade <= 18) {
      this.mensagem[0] = "Abaixo do peso!"
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc >= 17.5 && valor_imc <= 25 && this.idade <= 18) {
      this.mensagem[0] = "no seu peso ideal, Parabéns!!!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc >= 25.1 && valor_imc <= 28.6 && this.idade <= 18) {
      this.mensagem[0] = "Pouco acima do peso!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc > 28.7 && this.idade <= 18) {
      this.mensagem[0] = "Obesidade!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc < 20.7 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Abaixo do peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc >= 20.7 && valor_imc <= 26.4 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "no seu peso ideal, Parabéns!!!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc > 26.4 && valor_imc <= 27.8 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Pouco acima do peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc > 27.8 && valor_imc <= 31.1 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Acima do Peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc > 31.1 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Obesidade!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc < 22 && this.idade > 59) {
      this.mensagem[0] = "Abaixo do peso!";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    } else if (valor_imc >= 22 && valor_imc <= 27 && this.idade > 59) {
      this.mensagem[0] = "Peso Ideal";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    } else if (valor_imc >= 27 && this.idade > 59) {
      this.mensagem[0] = "Soprepeso!";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    }
  }

  imc_feminino(valor_imc: number) {
    if (valor_imc < 19.1 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Abaixo do peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc >= 19.1 && valor_imc <= 25.9 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "no seu peso ideal, Parabéns!!!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc >= 25.9 && valor_imc <= 27.3 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Pouco acima do peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc >= 27.3 && valor_imc <= 32.3 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Acima do Peso!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc > 32.3 && this.idade >= 19 && this.idade <= 59) {
      this.mensagem[0] = "Obesidade!";
      this.mensagem[1] = "Pertence a tabela Adulta";
    } else if (valor_imc < 16.7 && this.idade <= 18) {
      this.mensagem[0] = "Abaixo do peso!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc >= 16.7 && valor_imc < 24.9 && this.idade <= 18) {
      this.mensagem[0] = "no seu peso ideal, Parabéns!!!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc >= 24.9 && valor_imc < 28.9 && this.idade <= 18) {
      this.mensagem[0] = "Pouco acima do peso!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc >= 28.9 && this.idade <= 18) {
      this.mensagem[0] = "Obesidade!";
      this.mensagem[1] = "Pertence a tabela de Crianças e Jovens";
    } else if (valor_imc < 22 && this.idade > 59) {
      this.mensagem[0] = "Abaixo do peso!";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    } else if (valor_imc >= 22 && valor_imc <= 27 && this.idade >= 59) {
      this.mensagem[0] = "Peso Ideal";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    } else if (valor_imc > 27 && this.idade > 59) {
      this.mensagem[0] = "Soprepeso!";
      this.mensagem[1] = "Pertence a tabela de Idosos";
    }
  }
}
